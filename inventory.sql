-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2019 at 09:33 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `batches`
--

CREATE TABLE `batches` (
  `id` int(11) NOT NULL,
  `batch_desc` varchar(500) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batches`
--

INSERT INTO `batches` (`id`, `batch_desc`, `date_added`) VALUES
(1, 'To print', '2019-11-22 14:09:38');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `brand` varchar(100) NOT NULL,
  `category` varchar(100) NOT NULL,
  `size` varchar(50) NOT NULL,
  `batch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `brand`, `category`, `size`, `batch_id`) VALUES
(1, 'Black Blazer with white dots and wide sleeve and round neck', 'NINE WEST', 'jackets and sweaters', '12', 1),
(2, 'Black Blazer with white dots and wide sleeve and round neck', 'NINE WEST', 'jackets and sweaters', '14', 1),
(3, 'Black Blazer with white dots and wide sleeve and round neck', 'NINE WEST', 'jackets and sweaters', '16', 1),
(4, 'Orange,red,white blazer with one button', 'KASPER', 'jackets and sweaters', '16', 1),
(5, 'Orange,red,white blazer with one button', 'KASPER', 'jackets and sweaters', '18', 1),
(6, 'Black with small white flowers,2 slits at the back', 'BGN', 'skirts ', 'XS', 1),
(7, 'Black with small white flowers,2 slits at the back', 'BGN', 'skirts ', 'S', 1),
(8, 'Brown suede skirt with front zip', 'EVITA', 'skirts ', '42', 1),
(9, 'Brown suede skirt with front zip', 'EVITA', 'skirts ', '44', 1),
(10, 'Black with white stripe and zip on side', 'SETRE', 'skirts ', 'MEDIUM', 1),
(11, 'Dark blue with 2 pleats on sides large belt on waist', 'CITAYA', 'skirts ', '48', 1),
(12, 'Dark blue with 2 pleats on sides large belt on waist', 'CITAYA', 'skirts ', '50', 1),
(13, 'Navy blue front pleat added piece on bottom,red belt', 'VEROM', 'skirts ', '42', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reorders`
--

CREATE TABLE `reorders` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_ordered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_filled` timestamp NULL DEFAULT NULL,
  `reorder_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reorders`
--

INSERT INTO `reorders` (`id`, `product_id`, `date_ordered`, `date_filled`, `reorder_status`) VALUES
(4, 1, '2019-07-11 14:38:57', '2019-07-11 15:19:04', 1),
(5, 3, '2019-07-11 15:24:30', '2019-07-11 15:40:05', 1),
(6, 2, '2019-07-11 15:32:12', NULL, 0),
(7, 1, '2019-07-11 15:40:38', '2019-07-11 15:40:50', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `tx_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `batches`
--
ALTER TABLE `batches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reorders`
--
ALTER TABLE `reorders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `batches`
--
ALTER TABLE `batches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `reorders`
--
ALTER TABLE `reorders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
