<?php
function db()
{
	$li = mysqli_connect("localhost","root","");
	mysqli_select_db($li,"inventory");
	return $li;
	
}

function get_sold($batch_id,$category)
{
	$l = db();
	$category = mysqli_real_escape_string($l,$category);
	$sql = "SELECT count(*) as tot FROM `sales` where product_id in ((select id from products where batch_id='$batch_id' and category='$category')) ";
	$re = mysqli_query($l,$sql);
	$res = array();
	$re2 = mysqli_fetch_array($re);
	
	return $re2['tot'];
	
	
}

function get_sales_by_size($batch_id,$category,$name,$brand,$size)
{
	$l = db();
	$name = mysqli_real_escape_string($l,$name);
	$category = mysqli_real_escape_string($l,$category);
	$brand = mysqli_real_escape_string($l,$brand);
	$size = mysqli_real_escape_string($l,$size);
	$sql = "SELECT count(*) as tot FROM `sales` where product_id in (select id from products where batch_id='$batch_id' and category='$category' and brand='$brand' and size='$size' and name='$name')";
	$re = mysqli_query($l,$sql);
	$res = array();
	$re2 = mysqli_fetch_array($re);
	
	return $re2['tot'];
}

function get_sales_by_product_id($id)
{
	$l = db();
	$sql = "SELECT count(*) as tot FROM `sales` where product_id = '$id' ";
	$re = mysqli_query($l,$sql);
	$res = array();
	$re2 = mysqli_fetch_array($re);
	
	return $re2['tot'];
}	

function add_product($batch_id,$category,$name,$size,$brand)
{
	
	$l = db();
	$name = mysqli_real_escape_string($l,$name);
	$category = mysqli_real_escape_string($l,$category);
	$brand = mysqli_real_escape_string($l,$brand);
	$size = mysqli_real_escape_string($l,$size);
    $sql = "insert into products values('','$name','$brand','$category','$size','$batch_id')";

	$re = mysqli_query($l,$sql);
	if(!$re)
	{
		echo mysqli_error($l);
		echo "--".$sql."--";
	}
	
	
}

function get_batches()
{
	$sql = "select * from batches";
	$l = db();
	$re = mysqli_query($l,$sql);
	$res = array();
	while($re2 = mysqli_fetch_array($re))
	{
		$res[] = $re2['batch_desc'];
		
	}
	return $res;
	
}

function product_details($id)
{
	$sql = "select * from products where id='$id'";
	$l = db();
	$re = mysqli_query($l,$sql);
	
	$re2 = mysqli_fetch_array($re);
	return $re2;
	
}

function get_batch_desc($id)
{
	$sql = "select * from batches where id='$id'";
	$l = db();
	$re = mysqli_query($l,$sql);
	//$res = array();
	while($re2 = mysqli_fetch_array($re))
	{
		$res = $re2['batch_desc'];
		
	}
	return $res;
	
}

function new_sale($id)
{
	$sql = "insert into sales values('','$id',now())";
	$l = db();
	$re = mysqli_query($l,$sql);
	if(!$re)
	{
		echo mysqli_error($l);
	}
	
}

function new_batch($name)
{
	$sql = "insert into batches values ('','$name',now())";
	$l = db();
	$re = mysqli_query($l,$sql);
	if(!$re)
	{
		echo mysqli_error($l);
	}
	return mysqli_insert_id($l);
	
}

function sale($id)
{
	$l = db();
	$sql = "select * from products where id='$id'";
	$re = mysqli_query($l,$sql);
	$re2 = mysqli_fetch_array($re);
	 $min_qtty = $re2['min_quantity']; 
	 $reorder_qtty = $re2['reorder_quantity']; 
	 $qtty = $re2['quantity']; 
	 $new_qtty = $qtty - 1;
	if($new_qtty < 0) //dont sell non existent items
	{
		return "Item out of stock";
		
	}
	$resp = "";
	if($new_qtty <= $min_qtty)//make order
	{
		$sql = "select * from reorders where product_id='$id' and reorder_status != 1";
	    $re = mysqli_query($l,$sql);
	    if(mysqli_num_rows($re) > 0) //existing reorder request not filled
		{
			$resp .= "Reorder request pending. ";
		}
		else
		{
			$sql = "insert into reorders set product_id='$id'";
		    mysqli_query($l,$sql);
		    $resp .= "Reorder request sent. ";
		}
		
	}
	$sql = "update products set quantity = quantity - 1 where id ='$id'";
	mysqli_query($l,$sql);
	$resp .= "Sale successfull";
	return $resp;
	
}


function get_reorder_qtty($id)
{
	$l = db();
	$sql = "select * from products where id='$id'";
	$re = mysqli_query($l,$sql);
	$re2 = mysqli_fetch_array($re);
	$reorder_qtty = $re2['reorder_quantity']; 
	return $reorder_qtty;
		
}

function get_name($id)
{
	$l = db();
	$sql = "select * from products where id='$id'";
	$re = mysqli_query($l,$sql);
	$re2 = mysqli_fetch_array($re);
	$name = $re2['name']; 
	return $name;
		
}

function get_prod_id($id)
{
	$l = db();
	$sql = "select * from reorders where id='$id'";
	$re = mysqli_query($l,$sql);
	$re2 = mysqli_fetch_array($re);
	$id = $re2['product_id']; 
	return $id;
		
}
function reorder($id)
{
	
	$l = db();
	$sql = "select * from reorders where id='$id' and reorder_status = 1";
	$re = mysqli_query($l,$sql);
	if(mysqli_num_rows($re) > 0) //reorder request filled
	{
		$resp = "Reorder request already filled. ";
		return $resp;
	}
	$pid = get_prod_id($id);
	$qtty = get_reorder_qtty($pid);
	$sql = "update products set quantity = quantity + $qtty where id = '$pid'";
	$re = mysqli_query($l,$sql);
	$resp = "Error filling the reorder";
	if($re)
	{
		$sql = "update reorders set date_filled=now(), reorder_status=1 where id = '$id'";
		$re2 = mysqli_query($l,$sql);
	}
	if(@$re2)
	{
		$resp = "Reorder succefully filled";
	}
	//check here and do commit/rollback
    return $resp;	
	
	
}


?>