<?php
ini_set('max_execution_time', 0);
require 'vendor/autoload.php';

		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
		use PhpOffice\PhpSpreadsheet\Writer\Xls;
require_once('funcs.php');
if(strlen(@$_POST['batch_id']) > 0)
	{
		extract($_POST);
	   $spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle('Barcodes');
		$sql = "select * from products where batch_id='$batch_id'";	     
		$l = db();
		$re = mysqli_query($l,$sql);
				echo mysqli_error($l);

		$res = array();
		$i = 2;
		 $sheet->setCellValue('A1', 'id');
		     $sheet->setCellValue('B1', 'name');
			 $sheet->setCellValue('C1', 'size');
		while($row = mysqli_fetch_array($re))
		{
			 $sheet->setCellValue('A'.$i, $row['id']);
		     $sheet->setCellValue('B'.$i, $row['name']);
			 $sheet->setCellValue('C'.$i, $row['size']);
		     $i++;
			// var_dump($row);
		}
				echo mysqli_error($l);
        
		// OUTPUT
		$fname = get_batch_desc($batch_id);
		$writer = new Xls($spreadsheet);
        header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$fname.'.xls"');
		
		header('Cache-Control: max-age=0');
		
		$writer->save('php://output');
	   
	  
	}

?>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Inventory</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="vendor/DataTables/datatables.min.css"/>
 

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark static-top bg-primary">
    <div class="container">
      <a class="navbar-brand" href="#">Victoria Fashions</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Sale
              <span class="sr-only">(current)</span>
            </a>
          </li>
		  <li class="nav-item active">
            <a class="nav-link" href="index.php?id=2">New Stocks
              <span class="sr-only">(current)</span>
            </a>
          </li>
		 
		  <li class="nav-item active">
            <a class="nav-link" href="index.php?id=3">Print Barcodes
              <span class="sr-only">(current)</span>
            </a>
          </li>
		  <li class="nav-item active">
            <a class="nav-link" href="index.php?id=4">Sales Report
              <span class="sr-only">(current)</span>
            </a>
          </li>
		  <li class="nav-item active">
            <a class="nav-link" href="index.php?id=5">Stock Report
              <span class="sr-only">(current)</span>
            </a>
          </li>
		  <li class="nav-item active">
            <a class="nav-link" href="index.php?id=9">Brand Report
              <span class="sr-only">(current)</span>
            </a>
          </li>
		  
         
        </ul>
      </div>
    </div>
  </nav>
<?php
extract($_GET);
if(@!$id)
{
	$id = "1";
}
if($id == "1")
{
	if(strlen(@$_POST['item']) > 0)
	{
	   new_sale($_POST['item']);
	   ?>
	   <br>
	   <br>
	   <div class="alert alert-success" role="alert">
          Sale successfully recorded
       </div>
	   
	   <?php
	}
	?>
	<!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
	  <br>
	  <h1 class="mt-5">New Sale</h1>
	  <form method="post" action="index.php?id=1">
       <div class="input-group input-group-lg">
         <div class="input-group-prepend">
           <span class="input-group-text" id="inputGroup-sizing-lg">Scan Item</span>
         </div>
         <input type="text" name="item" required autofocus class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg">
      </div>
	  </form>
	  <div class="clearfix"></div>
	  <br>
	  

     </div>
    </div>
  </div>
<!-- End page content -->
	
	
<?php
}
if($id == "3")
{
		
	?>
	<!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
	  <br>
	  <h1 class="mt-5">Generate Barcode File</h1>
	  <form method="post" action="index.php?id=3">
       <select class="custom-select custom-select-lg mb-3" name='batch_id' onchange='submit()'>
  <option selected>Select the Batch</option>
  <?php
  $sql = "select * from batches";
	$l = db();
	$re = mysqli_query($l,$sql);
	$res = array();
	while($re2 = mysqli_fetch_array($re))
	{
		echo "<option value=".$re2['id'].">". $re2['batch_desc']."</option>";	
	}
  
  ?>
  
</select>
	  </form>
	  <div class="clearfix"></div>
	  <br>
	  

     </div>
    </div>
  </div>
<!-- End page content -->
	
	
	
	
	<?php
}
if($id == "2")
{
	if(@$_POST['upload'])
	{
		//print_r($_POST);
		$extension = pathinfo($_FILES['fileURL']['name'], PATHINFO_EXTENSION);
 
                if($extension == 'csv'){
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
                } elseif($extension == 'xlsx') {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                } else {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                }
                // file path
                $spreadsheet = $reader->load($_FILES['fileURL']['tmp_name']);
		$all = $spreadsheet->getSheetNames();
		//var_dump($all);
		$c = count($all);
		$i = 0;
		$batch_id = new_batch($_POST['batch']);
		while($i < $c)
		{
			$data = $spreadsheet->getSheet($i)->toArray();
			
			//
			//echo "<pre>";
			//print_r($data);
			//echo "</pre>";
			$j = 0;
			while($j < count($data))
			{
				$names = $data[$j][1];
				$size = $data[$j][2];
				if($names && $data[$j][0] && (strtoupper($data[$j][0]) != "BRAND NAME") )
				{
					
				    add_product($batch_id,$all[$i],$data[$j][1],$data[$j][2],$data[$j][0]);
				}
				$j++;
			}
			
			
			$i++;
		}
		echo '<div class="alert alert-success">Data successfully uploaded. </div>';

		
	}

?>
  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
	  <h1 class="mt-5">New Stocks</h1>
	  <form action="index.php?id=2" method="post" enctype="multipart/form-data">
       <div class="input-group input-group-lg">
         <div class="input-group-prepend">
           <span class="input-group-text" id="inputGroup-sizing-lg">Batch Description</span>
         </div>
         <input type="text" required name="batch" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg">
      </div>
	  <div class="clearfix"></div>
	  <br>
	  
 <div class="clearfix"></div>
	  <br>
	  <div class="input-group mb-3">
        <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroupFileAddon01">Upload Excel File </span>
  </div>
  <div class="custom-file">
    <input type="file" class="custom-file-input"  id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="fileURL">
    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
  </div>
</div>
<br>
       
       <input type="submit" class="btn btn-success btn-lg" value="Upload Stocks" name="upload">
	  </form> 
     </div>
    </div>
  </div>
<!-- End page content -->


<?php	
	
}
if($id == "4")
{
	
	
	?>
	<!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
	  <br>
	  <h1 class="mt-5">Sales Report</h1>
	  
	  <form method="post" action="index.php?id=4">
	  </div>
	  <div class="col-lg-4">
       <div class="form-group">
 <label ><b>Date From</b></label>
 <input type="date" name="from" max="3000-12-31" 
        min="1000-01-01" class="form-control">
</div>
</div>
<div class="col-lg-4">
<div class="form-group">
 <label ><b>Date To</b></label>
 <input type="date" name="to" min="1000-01-01"
        max="3000-12-31" class="form-control" >
</div>
</div>
<div class="col-lg-4">
<label ><b>&nbsp;</b></label>
<input type="submit" class="form-control btn btn-primary btn-lg" value="View Report" name="view">
</div>
</div>
	  </form>
	  <div class="clearfix"></div>
	  <br>
	  

     </div>
    
  </div>
<!-- End page content -->
	 <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
	  <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
			    <th>Date</th>
                <th>Category</th>
                <th>Batch</th>
                <th>Item</th>
				<th>Size</th>
                
                
            </tr>
        </thead>
        <tbody>
		<?php
		extract($_POST);
		$l = db();
		@$sql = "SELECT * from sales where tx_date between '$from' and '$to' order by tx_date desc ";
		$re = mysqli_query($l,$sql);		
		while($re2 = mysqli_fetch_array($re))
		{
			
		$det = product_details($re2['product_id']);	
		
        echo "<tr>";
		echo "<td>".$re2['tx_date']."</td>";
		//echo "<td>".$det['category']."</td>";
		echo "<td><a href=index.php?id=7&cat=".urlencode($det['category']).">".$det['category']."</a></td>";

		echo "<td><a href=index.php?id=6&bid=".$det['batch_id'].">".get_batch_desc($det['batch_id'])."</a></td>";
		echo "<td>".$det['name']."</td>";
		echo "<td>".$det['size']."</td>";
		
        echo "</tr>";
        }
        ?>		
        </tbody>
        <tfoot>
            <tr>
                <th>Date</th>
                <th>Category</th>
                <th>Batch</th>
                <th>Item</th>
				<th>Size</th>
            </tr>
        </tfoot>
    </table>
	  <?php
	if(@$_POST['view'])
	{
		//print_r($_POST);
	}
	?>
	   
     </div>
    </div>
  </div>
<!-- End page content -->
	
	<?php
	
}
if($id == "5")
{		
	?>
	 <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
	  <br>
	  <h1 class="mt-5">Stocks Report</h1>
	  <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
			    
                <th>Batch</th>
				<th>Category</th>
                <th>Opening Stock</th>
                <th>Current Stock</th>
				<th>Total Sold</th>
                    
            </tr>
        </thead>
        <tbody>
		<?php
		extract($_POST);
		$l = db();
		@$sql = "SELECT batch_id,category,count(category) as opening_stock from products where batch_id in (1,2) group by category,batch_id order by batch_id desc ";
		$re = mysqli_query($l,$sql);	
        echo mysqli_error($l);		
		while($re2 = mysqli_fetch_array($re))
		{
			
		$open = $re2['opening_stock'];
		$sales = get_sold($re2['batch_id'],$re2['category']);
		$current = $open - $sales;
        echo "<tr>";
		echo "<td><a href=index.php?id=6&bid=".$re2['batch_id'].">".get_batch_desc($re2['batch_id'])."</a></td>";
        echo "<td><a href=index.php?id=7&cat=".urlencode($re2['category']).">".$re2['category']."</a></td>";
		echo "<td>".$open."</td>";
		echo "<td>".$current."</td>";
		echo "<td>".$sales."</td>";
		
        echo "</tr>";
        }
        ?>		
        </tbody>
        <tfoot>
            <tr>
                <th>Batch</th>
				<th>Category</th>
                <th>Opening Stock</th>
                <th>Current Stock</th>
				<th>Total Sold</th>
            </tr>
        </tfoot>
    </table>
	  <?php
	if(@$_POST['view'])
	{
		//print_r($_POST);
	}
	?>
	   
     </div>
    </div>
  </div>
<!-- End page content -->
	
	<?php
	
}

if($id == "6")
{		
$bid = $_GET['bid'];
	?>
	 <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
	  <br>
	  <h1 class="mt-5">Batch: <?php echo get_batch_desc($bid); ?> Report</h1>
	  <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
			                  
				<th>Category</th>
				<th>Brand</th>
				<th>Name</th>
				<th>Size</th>
                <th>Opening Stock</th>
                <th>Current Stock</th>
				<th>Total Sold</th>
                    
            </tr>
        </thead>
        <tbody>
		<?php
		extract($_POST);
		$l = db();
		@$sql = "SELECT count(size) as opening,name,brand,category,size FROM `products` where batch_id='$bid' group by name,brand,category,size";
		$re = mysqli_query($l,$sql);	
        echo mysqli_error($l);		
		while($re2 = mysqli_fetch_array($re))
		{
		
		$sales = get_sales_by_size($bid,$re2['category'],$re2['name'],$re2['brand'],$re2['size']);
		$open = $re2['opening'];
		$current = $open - $sales;
        echo "<tr>";
		echo "<td><a href=index.php?id=7&cat=".urlencode($re2['category']).">".$re2['category']."</a></td>";
        echo "<td>".$re2['brand']."</td>";
		echo "<td>".$re2['name']."</td>";
		echo "<td>".$re2['size']."</td>";
		echo "<td>".$open."</td>";
		echo "<td>".$current."</td>";
		echo "<td>".$sales."</td>";
		
        echo "</tr>";
        }
        ?>		
        </tbody>
        <tfoot>
            <tr>
                <th>Category</th>
				<th>Brand</th>
				<th>Name</th>
				<th>Size</th>
                <th>Opening Stock</th>
                <th>Current Stock</th>
				<th>Total Sold</th>
            </tr>
        </tfoot>
    </table>
	  <?php
	if(@$_POST['view'])
	{
		//print_r($_POST);
	}
	?>
	   
     </div>
    </div>
  </div>
<!-- End page content -->
	
	<?php
	
}

if($id == "7") //category report
{		

	?>
	 <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
	  <br>
	  <h1 class="mt-5">Category: <?php echo $cat; ?> Report</h1>
	  <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
			                  
				<th>Batch</th>
				<th>Brand</th>
				<th>Name</th>
				<th>Size</th>
                <th>Opening Stock</th>
                <th>Current Stock</th>
				<th>Total Sold</th>
                    
            </tr>
        </thead>
        <tbody>
		<?php
		extract($_POST);
		$l = db();
		$cat = mysqli_real_escape_string($l,$cat);
		$sql = "SELECT count(size) as opening,name,brand,batch_id,category,size FROM `products` where category='$cat' group by name,brand,batch_id,size ORDER BY `batch_id` DESC ";
		$re = mysqli_query($l,$sql);	
        echo mysqli_error($l);
		$total = 0;	
		while($re2 = mysqli_fetch_array($re))
		{
		
		$sales = get_sales_by_size($re2['batch_id'],$re2['category'],$re2['name'],$re2['brand'],$re2['size']);
		$open = $re2['opening'];
		$total += $open;
		$current = $open - $sales;
        echo "<tr>";
		echo "<td><a href=index.php?id=6&bid=".$re2['batch_id'].">".get_batch_desc($re2['batch_id'])."</a></td>";
        echo "<td><a href=index.php?id=8&b=".urlencode($re2['brand']).">".$re2['brand']."</a></td>";
		echo "<td>".$re2['name']."</td>";
		echo "<td>".$re2['size']."</td>";
		echo "<td>".$open."</td>";
		echo "<td>".$current."</td>";
		echo "<td>".$sales."</td>";
		
        echo "</tr>";
        }
        ?>		
        </tbody>
        <tfoot>
            <tr>
                <th>Batch</th>
				<th>Brand</th>
				<th>Name</th>
				<th>Size</th>
                <th>Opening Stock</th>
                <th>Current Stock</th>
				<th>Total Sold</th>
            </tr>
        </tfoot>
    </table>
	<h1 class="mt-5"><?php echo  "Total items: ".number_format($total); ?> </h1>
	  <?php
	if(@$_POST['view'])
	{
		//print_r($_POST);
	}
	?>
	   
     </div>
    </div>
  </div>
<!-- End page content -->
	
	<?php
	
}

if($id == "8") //category report
{		

	?>
	 <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
	  <br>
	  <h1 class="mt-5">Brand: <?php echo $b; ?> Report</h1>
	  <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
			                  
				<th>Batch</th>
				<th>Category</th>
				<th>Name</th>
				<th>Size</th>
                <th>Opening Stock</th>
                <th>Current Stock</th>
				<th>Total Sold</th>
                    
            </tr>
        </thead>
        <tbody>
		<?php
		extract($_POST);
		$l = db();
		$b = mysqli_real_escape_string($l,$b);
		$sql = "SELECT count(size) as opening,name,brand,batch_id,category,size FROM `products` where brand='$b' group by name,brand,batch_id,size ORDER BY `batch_id` DESC ";
		$re = mysqli_query($l,$sql);	
        echo mysqli_error($l);
		$total = 0;	
		while($re2 = mysqli_fetch_array($re))
		{
		
		$sales = get_sales_by_size($re2['batch_id'],$re2['category'],$re2['name'],$re2['brand'],$re2['size']);
		$open = $re2['opening'];
		$total += $open;
		$current = $open - $sales;
        echo "<tr>";
		echo "<td><a href=index.php?id=6&bid=".$re2['batch_id'].">".get_batch_desc($re2['batch_id'])."</a></td>";
        echo "<td><a href=index.php?id=7&cat=".urlencode($re2['category']).">".$re2['category']."</a></td>";
		echo "<td>".$re2['name']."</td>";
		echo "<td>".$re2['size']."</td>";
		echo "<td>".$open."</td>";
		echo "<td>".$current."</td>";
		echo "<td>".$sales."</td>";
		
        echo "</tr>";
        }
        ?>		
        </tbody>
        <tfoot>
            <tr>
                <th>Batch</th>
				<th>Brand</th>
				<th>Name</th>
				<th>Size</th>
                <th>Opening Stock</th>
                <th>Current Stock</th>
				<th>Total Sold</th>
            </tr>
        </tfoot>
    </table>
	<h1 class="mt-5"><?php echo  "Total items: ".number_format($total); ?> </h1>
	  <?php
	if(@$_POST['view'])
	{
		//print_r($_POST);
	}
	?>
	   
     </div>
    </div>
  </div>
<!-- End page content -->
	
	<?php
	
}
if($id == "9")
{
//	print_r($_POST);
	extract($_POST);
	?>
	<!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
	  <br>
	  <h1 class="mt-5">Brand Report</h1>
	  
	  <form method="post" action="index.php?id=9">
	  </div>
	  <div class="col-lg-4">
       <div class="form-group">
 <label ><b>Brand</b></label>
 <select name="brand" class="form-control" id="brand" onchange='submit()'>
 <option value=0 SELECTED>Select brand</option>
 <?php
 $l = db();
 $sql = "select distinct(brand) as b from products order by brand ";
 $re = mysqli_query($l,$sql);
 while($re2 = mysqli_fetch_array($re))
 {
	if($brand == $re2['b'])
	{
       echo "<option selected value=".$re2['b'].">".$re2['b']."</option>";
	}
	else
	{
	    echo "<option value=".$re2['b'].">".$re2['b']."</option>";
	}	
		
 }
 
 ?>
 
 </select>
</div>
</div>
<div class="col-lg-4">
<div class="form-group">
 <label ><b>Item</b></label>
 <select name="item" class="form-control" id="item">
 <option value=0>Select item</option>
 <option value=100>All Items</option>
 <?php
 $sql = "select distinct(name) as b from products where brand ='$brand'";
 $re = mysqli_query($l,$sql);
 while($re2 = mysqli_fetch_array($re))
 {
	
	echo "<option value=".urlencode($re2['b']).">".$re2['b']."</option>";
			
 }
 ?>
 </select>
</div>
</div>
<div class="col-lg-4">
<label ><b>&nbsp;</b></label>
<input type="submit" class="form-control btn btn-success btn-lg" value="View Report" name="view">
</div>
</div>
	  </form>
	  <div class="clearfix"></div>
	  <br>
	  

     </div>
    
  </div>
<!-- End page content -->
	 <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
	  <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
               <th>Category</th>
                <th>Batch</th>
                <th>Item</th>
				<th>Size</th>
				<th>Opening Stock</th>
                <th>Current Stock</th>
				<th>Total Sold</th> 
                             
            </tr>
        </thead>
        <tbody>
		<?php
		extract($_POST);
		//print_r($_POST);
		$l = db();
		
	    @$item = mysqli_real_escape_string($l,urldecode($item));
	    @$brand = mysqli_real_escape_string($l,urldecode($brand));
		if($item == "100")
		{
			$sql = "SELECT count(size) as opening,name,brand,batch_id,category,size FROM `products` where brand='$brand' group by name,brand,batch_id,size ORDER BY `batch_id` DESC ";

		}
		else
		{
		  $sql = "SELECT count(size) as opening,name,brand,batch_id,category,size FROM `products` where brand='$brand' and name = '$item' group by name,brand,batch_id,size ORDER BY `batch_id` DESC ";
		}
		$re = mysqli_query($l,$sql);
		$to_sales = 0;
		$to_open = 0;	
		while($re2 = mysqli_fetch_array($re))
		{
		
        $sales = get_sales_by_size($re2['batch_id'],$re2['category'],$re2['name'],$re2['brand'],$re2['size'])	;	
		$to_sales += $sales;
		$open = $re2['opening'];
		$to_open  += $open;
		$current = $open - $sales;
        echo "<tr>";
		
		echo "<td><a href=index.php?id=7&cat=".urlencode($re2['category']).">".$re2['category']."</a></td>";

		echo "<td><a href=index.php?id=6&bid=".$re2['batch_id'].">".get_batch_desc($re2['batch_id'])."</a></td>";
		echo "<td>".$re2['name']."</td>";
		echo "<td>".$re2['size']."</td>";
		echo "<td>".$open."</td>";
		echo "<td>".$current."</td>";
		echo "<td>".$sales."</td>";
		
        echo "</tr>";
        }
        ?>		
        </tbody>
        <tfoot>
            <tr>
			<th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
				<th>Totals</th>
				<th><?php echo $to_open; ?></th>
                <th><?php echo $to_open - $to_sales; ?></th>
				<th><?php echo $to_sales; ?></th>
				
                
            </tr>
        </tfoot>
    </table>
	  <?php
	if(@$_POST['view'])
	{
		//print_r($_POST);
	}
	?>
	   
     </div>
    </div>
  </div>
<!-- End page content -->
	
	<?php
	
}

?>
  
  
  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script type="text/javascript" src="vendor/DataTables/datatables.min.js"></script>

 <script type="text/javascript"> 
 $(document).ready(function() {
    $('#example').DataTable();
} );
$('#example2').DataTable( {
    drawCallback: function () {
      var api = this.api();
      $( api.table().footer() ).html(
        api.column( 4, {page:'current'} ).data().sum()
      );
    }
  } );
 
 function update(id)
 {
	 $.ajax({
            url : 'process.php?sid=' + id,
            type: "GET",
            data: $(this).serialize(),
            success: function (data) {
			   alert(data);
            },
            error: function (jXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
	 
	 
 }

</script>
</body>

</html>
<?php



?>
